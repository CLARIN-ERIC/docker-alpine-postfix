#!/usr/bin/env bash

set -e

HOST=${HOST:-myhostname}
DOMAIN=${DOMAIN:-example.com}
ROOT_EMAIL=${ROOT_EMAIL:-user@${DOMAIN}}
SMTP_ADDR=${SMTP_ADDR:-smtp.example.email:587}
SMTP_USER=${SMTP_USER:-the_username}
SMTP_PWD=${SMTP_PWD:-the_password}

echo "Customizing postfix configuration..."

postconf -e "smtputf8_enable=no"
postconf -e "inet_protocols=ipv4"
postconf -e "mydomain=${DOMAIN}"
postconf -e "myhostname=mail"
postconf -e "myorigin=\$myhostname"
postconf -e "mydestination=\$myhostname,localhost.\$mydomain,localhost"
postconf -e "virtual_alias_maps=regexp:/etc/postfix/virtual"
postconf -e "sender_canonical_maps=regexp:/etc/postfix/canonical"
postconf -e "smtp_header_checks=regexp:/etc/postfix/header_checks"
postconf -e "mynetworks=127.0.0.0/8 172.16.0.0/12"
postconf -e "remote_header_rewrite_domain=\$mydomain"
postconf -e "relayhost=vps.transip.email:587"
postconf -e "smtp_sasl_auth_enable=yes"
postconf -e "smtp_sasl_security_options=noanonymous"
postconf -e "smtp_sasl_password_maps = lmdb:/etc/postfix/sasl_passwd"
postconf -e "smtp_use_tls=yes"
postconf -e "smtp_tls_security_level=encrypt"
postconf -e "smtp_tls_note_starttls_offer=yes"
printf '%s\n' "/^root@${DOMAIN}$/ ${ROOT_EMAIL}" | tee -a /etc/postfix/virtual &> /dev/null
printf '%s\n' "/^(.+)@(\w+)$/ @${DOMAIN}" | tee -a /etc/postfix/virtual &> /dev/null
printf '%s\n' "/^(.+)@(\w+)$/ docker-\${1}_at_\${2}_on_${HOST}@${DOMAIN}" | tee -a /etc/postfix/canonical &> /dev/null
printf '%s\n' "/^From:[[:space:]]+docker\-(.+)_at_(.+)_on_(.+)@(.+)/ REPLACE From: \"Docker mail [\${1}@\${2}] on \${3}\" <docker-\${1}_at_\${2}_on_\${3}@\${4}>" | tee -a /etc/postfix/header_checks &> /dev/null
printf '%s\n' "${SMTP_ADDR} ${SMTP_USER}:${SMTP_PWD}" | tee -a /etc/postfix/sasl_passwd &> /dev/null
postmap /etc/postfix/virtual
postmap /etc/postfix/canonical
postmap /etc/postfix/header_checks
postmap lmdb:/etc/postfix/sasl_passwd
postalias /etc/postfix/aliases
chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.lmdb
chmod 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.lmdb
chown -v root /var/spool/postfix /var/spool/postfix/pid
chown -Rv postfix:postfix /var/lib/postfix

echo "Customization complete"
