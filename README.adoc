= Dockerized Postfix mail relay agent for the CLARIN infrastructure
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker image used in the CLARIN infrastructure which provides a relay agent for the various containers' generated email messages. This email relay agent is based on Postfix and relies on a external SMTP server to deliver its messages. Processes are managed by supervisord and log aggregation is handled with fluentd.

== Configuration

=== Environment variables[[anchor-1]]
- HOST: The hostname of the host where the container is running. To be included in the `sender` field 
- DOMAIN: Domain from which the sender email will send its messages.
- ROOT_EMAIL: email address to where system messages are delivered to.
- SMTP_ADDR: SMTP address of the external relay server.
- SMTP_USER: SMTP login name for the external relay server.
- SMTP_PWD: SMTP password for the external relay server.

=== Inherited configuration

Configuration inherited from the https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[base image]:

- https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/crond/root-crontab[root crontab]:
periodically runs a https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/crond/clean_logs.sh[cleanup script] to clean old log files.
- https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/tree/master/image/fluentd[fluentd logging configurations] for:
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/fluentd/fluentd.conf[Main process]
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/fluentd/crond.conf[Cron jobs]
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/fluentd/init.conf[Init script]
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/fluentd/messages.conf[System messages]
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/fluentd/supervisord.conf[supervisord]
- https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/tree/master/image/supervisor[Supervisord launch configurations] for:
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/supervisor/supervisord.conf[Main process]
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/supervisor/crond.conf[crond]
* https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base/blob/master/image/supervisor/rsyslogd.conf[rsyslogd] used for system log

=== Usage
==== To setup this image as your email transfer agent container:
Provide it with the environment variables described in <<anchor-1, 1.1>> . A `docker-compose` based example can be found https://gitlab.com/CLARIN-ERIC/compose_clarin-postfix/blob/master/clarin/docker-compose.yml[here].

==== To setup other containers to use this image as their email transfer agent:

1. Make sure that busybox `sendmail` is available in your container and as not been overriden by other sendmail instalatons or wrappers (e.g. postfix-sendmailre)
2. Make sure that your container as well as the container created by this image, share the same Docker network
3. Provide your image with the following environment variable:

- SMTPHOST - The name of the container created by this image

A `docker-compose` based example can be found https://gitlab.com/CLARIN-ERIC/compose_clarin-nginx-infra/blob/master/clarin/docker-compose.yml[here].

=== Logging on the host

To be filled in.